local p_spead = Proto("spead","Spead")

local f_header_identifier_start = ProtoField.uint16("spead.header_start_bit", "Header Identifier start bit", base.HEX, nil, 0x0001)
local f_header_identifier = ProtoField.uint16("spead.header_identifier", "Header Identifier", base.HEX, nil, 0x7FFF)
local f_item_pointer_width = ProtoField.uint8("spead.item_pointer_width", "Item pointer width", base.DEC)
local f_heap_addr_width = ProtoField.uint8("spead.heap_addr_width", "Heap address width", base.DEC)
local f_item_id_width = ProtoField.uint8("spead.item_id_width", "Item ID width", base.DEC)
local f_reserved = ProtoField.uint8("spead.reserved", "Reserved", base.HEX)
local f_nof_items = ProtoField.uint8("spead.nof_items", "Number of Items in spead header", base.DEC)

local f_packet_counter = ProtoField.uint32("spead.packet_counter", "packet counter", base.DEC)
local f_lmc_packet_counter = ProtoField.uint32("spead.lmc_packet_counter", "packet counter", base.DEC, nil, 0x0FFF)


local f_ska_packet_counter_top = ProtoField.uint8("spead.ska_packet_counter_top", "packet counter since TAI 2000 top", base.HEX)
local f_ska_packet_counter_bottom = ProtoField.uint32("spead.ska_packet_counter_bottom", "packet counter since TAI 2000 bottom", base.HEX)

local f_logical_channel_id = ProtoField.uint16("spead.logical_channel_id", "logical channel id", base.DEC)
local f_header_contents_top = ProtoField.uint16("spead.header_contents_top", "header contents top",  base.HEX)
local f_header_contents_bottom = ProtoField.uint32("spead.header_contents_bottom", "header contents bottom",  base.HEX)

local f_beam_id = ProtoField.uint16("spead.beam_id", "beam id", base.DEC)
local f_frequency_id = ProtoField.uint16("spead.frequency_id", "frequency id", base.DEC)

local f_scan_id_top = ProtoField.uint16("spead.scan_id_top", "scan id top",  base.HEX)
local f_scan_id_bottom = ProtoField.uint32("spead.scan_id_bottom", "scan id bottom",  base.HEX)

local f_lmc_packet_payload_length_top = ProtoField.uint8("spead.lmc_packet_payload_length_top", "packet payload length top",  base.HEX)
local f_packet_payload_length_top = ProtoField.uint16("spead.packet_payload_length_top", "packet payload length top",  base.HEX)
local f_packet_payload_length_bottom = ProtoField.uint32("spead.packet_payload_length_bottom", "packet payload length bottom",  base.HEX)

local f_substation_id = ProtoField.uint8("spead.substation_id", "substation id", base.DEC)
local f_subarray_id = ProtoField.uint8("spead.subarray_id", "subarray id", base.DEC)
local f_station_id = ProtoField.uint16("spead.station_id", "station id", base.DEC)
local f_aperture_id = ProtoField.uint16("spead.aperture_id", "aperture id", base.DEC)
local f_nof_cont_ant = ProtoField.uint16("spead.nof_cont_ant", "number of contributing antenna", base.DEC)
local f_tpm_id = ProtoField.uint8("spead.tpm_id", "tpm id", base.DEC)
local f_fpga_id = ProtoField.uint8("spead.fpga_id", "fpga id", base.DEC)

local f_antenna_0_id = ProtoField.uint8("spead.antenna_0_id", "antenna 0 id", base.DEC)
local f_antenna_1_id = ProtoField.uint8("spead.antenna_1_id", "antenna 1 id", base.DEC)
local f_antenna_2_id = ProtoField.uint8("spead.antenna_2_id", "antenna 2 id", base.DEC)
local f_antenna_3_id = ProtoField.uint8("spead.antenna_3_id", "antenna 3 id", base.DEC)
local f_nof_included_antennas = ProtoField.uint8("spead.nof_included_antennas", "nof included antennas", base.DEC)


local f_lmc_payload_offset_top = ProtoField.uint8("spead.lmc_payload_offset_top", "payload offset top", base.HEX)
local f_payload_offset_top = ProtoField.uint16("spead.payload_offset_top", "payload offset top", base.HEX)
local f_payload_offset_bottom = ProtoField.uint32("spead.payload_offset_bottom", "payload offset bottom", base.HEX)

local f_lmc_sync_time_top = ProtoField.uint8("spead.lmc_sync_time_top", "sync time top", base.HEX)
local f_sync_time_top = ProtoField.uint16("spead.sync_time_top", "sync time top", base.HEX)
local f_sync_time_bottom = ProtoField.uint32("spead.sync_time_bottom", "sync time bottom", base.HEX)

local f_lmc_timestamp_top = ProtoField.uint8("spead.lmc_timestamp_top", "timestamp top", base.HEX)
local f_timestamp_top = ProtoField.uint16("spead.timestamp_top", "timestamp top", base.HEX)
local f_timestamp_bottom = ProtoField.uint32("spead.timestamp_bottom", "timestamp bottom", base.HEX)

local f_center_frequency_top = ProtoField.uint16("spead.center_frequency_top", "center frequency top", base.HEX)
local f_center_frequency_bottom = ProtoField.uint32("spead.center_frequency_bottom", "center frequency bottom", base.HEX)

local f_lmc_capture_mode_top = ProtoField.uint8("spead.lmc_capture_mode_top", "lmc capture mode top", base.HEX)
local f_lmc_capture_mode_bottom = ProtoField.uint32("spead.lmc_capture_mode_bottom", "lmc capture mode bottom", base.HEX)

local f_lmc_capture_mode = ProtoField.string("spead.lmc_capture_mode", "lmc capture mode", base.STRING)

local f_start_ant_id = ProtoField.uint8("spead.start_ant_id", "start antenna id", base.DEC)
local f_nof_included_ant = ProtoField.uint8("spead.nof_included_ant", "number of included antennas", base.DEC)

local f_start_channel_id = ProtoField.uint16("spead.start_channel_id", "start channel id", base.DEC)
local f_nof_included_channel = ProtoField.uint8("spead.nof_included_channel", "number of included channels", base.DEC)

local f_nof_included_channels = ProtoField.uint16("spead.nof_included_channels", "number of included channels", base.DEC)



p_spead.fields = { f_header_identifier_start, f_header_identifier, f_item_pointer_width, f_heap_addr_width, f_reserved, f_nof_items, f_packet_counter, f_lmc_packet_counter, f_ska_packet_counter_top, f_ska_packet_counter_bottom, f_logical_channel_id, f_header_contents_top, f_header_contents_bottom, f_beam_id, f_frequency_id, f_scan_id_top, f_scan_id_bottom, f_packet_payload_length_top, f_packet_payload_length_bottom, f_substation_id, f_subarray_id, f_station_id, f_aperture_id, f_nof_cont_ant, f_tpm_id, f_fpga_id, f_lmc_payload_offset_top, f_payload_offset_top, f_payload_offset_bottom, f_sync_time_top, f_sync_time_bottom, f_timestamp_top, f_timestamp_bottom, f_center_frequency_top, f_center_frequency_bottom, f_lmc_capture_mode_top, f_lmc_capture_mode_bottom, f_start_ant_id, f_nof_included_ant, f_start_channel_id, f_nof_included_channel, f_lmc_packet_payload_length_top, f_item_id_width, f_lmc_capture_mode, f_lmc_sync_time_top, f_lmc_timestamp_top, f_nof_included_channels,
f_antenna_0_id, f_antenna_1_id, f_antenna_2_id, f_antenna_3_id, f_nof_included_antennas }

function p_spead.dissector(buffer, pinfo, tree)
    
    
    if buffer(0,2):uint() == 0x5304 then
    
        local subtree = tree:add(p_spead, buffer(), "SPEAD header")
        
        local offset = 0
        
        local header_start_tree = subtree:add(p_spead, buffer(offset,8), "Start of Header")
        
        header_start_tree:add(f_header_identifier, buffer(offset,2):uint())
        header_start_tree:add(f_item_pointer_width, buffer(offset+2,1):uint())
       
        
        local nof_items = buffer(offset+6,2):uint()
        
        local ska_spead_header = 0
        
        if nof_items == 6 then
            ska_spead_header = 1
        end
        
        local lmc = 0
        local lmc_offset = 0
        
        for i = 1, nof_items do
        
            local header_identifier = bit.band(buffer(lmc_offset,2):uint(),0x7FFF)

            if header_identifier == 0x2004 then
                lmc = 1
            end
            
            lmc_offset = lmc_offset + 8
        end
        
        if lmc == 1 then
            header_start_tree:add(f_item_id_width, buffer(offset+3,1):uint())
            pinfo.cols.protocol = "LMC_SPEAD"

        else
            header_start_tree:add(f_heap_addr_width, buffer(offset+3,1):uint())
            
            if ska_spead_header == 0 then
                pinfo.cols.protocol = "LFAA_CSP_SPEAD"
            else
                pinfo.cols.protocol = "SKA_CSP_SPEAD"
            end
            
        end

        header_start_tree:add(f_reserved, buffer(offset+4,2):uint())
        header_start_tree:add(f_nof_items, buffer(offset+6,2):uint())
        
        for i = 1, nof_items do
            offset = offset + 8
            
            local header_identifier = bit.band(buffer(offset,2):uint(),0x7FFF)
                    
            if header_identifier == 0x0001 then

                local heap_counter_tree = subtree:add(p_spead, buffer(offset,8), "Heap Counter")
                
                if ska_spead_header == 0 then
                    if lmc == 0 then
                        heap_counter_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                        heap_counter_tree:add(f_header_identifier, buffer(offset,2):uint())
                        heap_counter_tree:add(f_logical_channel_id, buffer(offset+2,2):uint())
                        heap_counter_tree:add(f_packet_counter, buffer(offset+4,4):uint())
                    else
                        heap_counter_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                        heap_counter_tree:add(f_header_identifier, buffer(offset,2):uint())
                        heap_counter_tree:add(f_lmc_packet_counter, buffer(offset+5,3):uint())
                    end
                    
                else
                    heap_counter_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                    heap_counter_tree:add(f_header_identifier, buffer(offset,2):uint())
                    heap_counter_tree:add(f_ska_packet_counter_top, buffer(offset+3,1):uint())
                    heap_counter_tree:add(f_ska_packet_counter_bottom, buffer(offset+4,4):uint())
                end
                
            elseif header_identifier == 0x0004 then
            
                local packet_length_tree = subtree:add(p_spead, buffer(offset,8), "Packet Length")

                packet_length_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                packet_length_tree:add(f_header_identifier, buffer(offset,2):uint())
                
                if lmc == 0 then
                    packet_length_tree:add(f_packet_payload_length_top, buffer(offset+2,2):uint())
                else
                    packet_length_tree:add(f_lmc_packet_payload_length_top, buffer(offset+3,1):uint())
                end
                
                packet_length_tree:add(f_packet_payload_length_bottom, buffer(offset+4,4):uint())
                
            elseif header_identifier == 0x1027 then
            
                local sync_time_tree = subtree:add(p_spead, buffer(offset,8), "Sync time") 
                sync_time_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                sync_time_tree:add(f_header_identifier, buffer(offset,2):uint())
                
                if lmc == 0 then
                    sync_time_tree:add(f_sync_time_top, buffer(offset+2,2):uint())
                else
                    sync_time_tree:add(f_lmc_sync_time_top, buffer(offset+3,1):uint())
                end
                
                sync_time_tree:add(f_sync_time_bottom, buffer(offset+4,4):uint())
            
            elseif header_identifier == 0x1600 then
            
                local timestamp_tree = subtree:add(p_spead, buffer(offset,8), "Timestamp") 
                timestamp_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                timestamp_tree:add(f_header_identifier, buffer(offset,2):uint())
                
                if lmc == 0 then
                    timestamp_tree:add(f_timestamp_top, buffer(offset+2,2):uint())
                else
                    timestamp_tree:add(f_lmc_timestamp_top, buffer(offset+3,1):uint())
                end
                timestamp_tree:add(f_timestamp_bottom, buffer(offset+4,4):uint())

            elseif header_identifier == 0x1011 then
            
                local center_freq_tree = subtree:add(p_spead, buffer(offset,8), "Center Frequency") 
                center_freq_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                center_freq_tree:add(f_header_identifier, buffer(offset,2):uint())
                center_freq_tree:add(f_center_frequency_top, buffer(offset+2,2):uint())
                center_freq_tree:add(f_center_frequency_bottom, buffer(offset+4,4):uint())

            elseif header_identifier == 0x3010 then
            
                local scan_id_tree = subtree:add(p_spead, buffer(offset,8), "Scan id") 
                scan_id_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                scan_id_tree:add(f_header_identifier, buffer(offset,2):uint())
                scan_id_tree:add(f_scan_id_top, buffer(offset+2,2):uint())
                scan_id_tree:add(f_scan_id_bottom, buffer(offset+4,4):uint())
                
            elseif header_identifier == 0x3000 then
            
                local channel_info_tree = subtree:add(p_spead, buffer(offset,8), "Channel Info")

                channel_info_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                channel_info_tree:add(f_header_identifier, buffer(offset,2):uint())
                
                if ska_spead_header == 1 then
                    channel_info_tree:add(f_logical_channel_id, buffer(offset+2,2):uint())
                end
                
                channel_info_tree:add(f_beam_id, buffer(offset+4,2):uint())
                channel_info_tree:add(f_frequency_id, buffer(offset+6,2):uint())
                
            elseif header_identifier == 0x3001 then
            
                local antenna_info_tree = subtree:add(p_spead, buffer(offset,8), "Antenna Info")

                antenna_info_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                antenna_info_tree:add(f_header_identifier, buffer(offset,2):uint())
                antenna_info_tree:add(f_substation_id, buffer(offset+2,1):uint())
                antenna_info_tree:add(f_subarray_id, buffer(offset+3,1):uint())
                antenna_info_tree:add(f_station_id, buffer(offset+4,2):uint())
                
                if ska_spead_header == 1 then
                    antenna_info_tree:add(f_aperture_id, buffer(offset+6,2):uint())
                else
                    antenna_info_tree:add(f_nof_cont_ant, buffer(offset+6,2):uint())
                end
            
            elseif header_identifier == 0x3300 then
            
                local sample_offset_tree = subtree:add(p_spead, buffer(offset,8), "Sample offset") 
                sample_offset_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                sample_offset_tree:add(f_header_identifier, buffer(offset,2):uint())
                sample_offset_tree:add(f_payload_offset_top, buffer(offset+2,2):uint())
                sample_offset_tree:add(f_payload_offset_bottom, buffer(offset+4,4):uint())

            elseif header_identifier == 0x2004 then
            
                local lmc_capture_mode_tree = subtree:add(p_spead, buffer(offset,8), "LMC capture mode")
                lmc_capture_mode_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                lmc_capture_mode_tree:add(f_header_identifier, buffer(offset,2):uint())
                
                local capture_mode_top = buffer(offset+3,1):uint()
                local capture_mode_bottom = buffer(offset+4,4):uint()
                
                local lmc_mode = "Unknown"
                
                if capture_mode_bottom == 0x0000 then
                    lmc_mode = "Raw ADC data"
                elseif capture_mode_bottom == 0x0001 then
                    lmc_mode = "Synchronized raw adc data"
                elseif capture_mode_bottom == 0x0004 then
                    lmc_mode = "Channel data"
                elseif capture_mode_bottom == 0x0005 then
                    lmc_mode = "Continuous channel data"
                elseif capture_mode_bottom == 0x0006 then
                    lmc_mode = "Integrated channel data"
                elseif capture_mode_bottom == 0x0008 then
                    lmc_mode = "Tile beam data"
                elseif capture_mode_bottom == 0x0009 then
                    lmc_mode = "Integrated tile beam"
                elseif capture_mode_bottom == 0x000C then
                    lmc_mode = "Antenna capture data"
                end
                
                lmc_capture_mode_tree:add(f_lmc_capture_mode, lmc_mode)
                lmc_capture_mode_tree:add(f_lmc_capture_mode_top, capture_mode_top)
                lmc_capture_mode_tree:add(f_lmc_capture_mode_bottom, capture_mode_bottom)
            
            elseif header_identifier == 0x2003 then
            
                local lmc_antenna_info = subtree:add(p_spead, buffer(offset,8), "LMC antenna info") 
                lmc_antenna_info:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                lmc_antenna_info:add(f_header_identifier, buffer(offset,2):uint())
                lmc_antenna_info:add(f_tpm_id, buffer(offset+3,1):uint())
                lmc_antenna_info:add(f_station_id, buffer(offset+4,2):uint())
                lmc_antenna_info:add(f_nof_cont_ant, buffer(offset+6,2):uint())
            
            elseif header_identifier == 0x2001 then
            
                local lmc_tpm_info = subtree:add(p_spead, buffer(offset,8), "LMC TPM info") 
                lmc_tpm_info:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                lmc_tpm_info:add(f_header_identifier, buffer(offset,2):uint())
                lmc_tpm_info:add(f_tpm_id, buffer(offset+3,1):uint())
                lmc_tpm_info:add(f_station_id, buffer(offset+4,2):uint())
                lmc_tpm_info:add(f_fpga_id, buffer(offset+7,1):uint())
                
            elseif header_identifier == 0x2002 then
            
                local lmc_channel_info = subtree:add(p_spead, buffer(offset,8), "LMC channel info") 
                lmc_channel_info:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                lmc_channel_info:add(f_header_identifier, buffer(offset,2):uint())
                lmc_channel_info:add(f_start_channel_id, buffer(offset+3,2):uint())
                lmc_channel_info:add(f_nof_included_channel, buffer(offset+5,1):uint())
                lmc_channel_info:add(f_start_ant_id, buffer(offset+6,1):uint())
                lmc_channel_info:add(f_nof_included_ant, buffer(offset+7,1):uint())
                
            elseif header_identifier == 0x2005 then

                local lmc_beam_info = subtree:add(p_spead, buffer(offset,8), "LMC beam info") 
                lmc_beam_info:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                lmc_beam_info:add(f_header_identifier, buffer(offset,2):uint())
                lmc_beam_info:add(f_beam_id, buffer(offset+3,1):uint())
                lmc_beam_info:add(f_start_channel_id, buffer(offset+4,2):uint())
                lmc_beam_info:add(f_nof_included_channels, buffer(offset+6,2):uint())
              
            elseif header_identifier == 0x2006 then     
                local lmc_antenna_info = subtree:add(p_spead, buffer(offset,8), "LMC antenna info") 
                lmc_antenna_info:add(f_antenna_0_id, buffer(offset+7,1):uint())
                lmc_antenna_info:add(f_antenna_1_id , buffer(offset+6,1):uint())
                lmc_antenna_info:add(f_antenna_2_id , buffer(offset+5,1):uint())
                lmc_antenna_info:add(f_antenna_3_id , buffer(offset+4,1):uint())
                lmc_antenna_info:add(f_nof_included_antennas, buffer(offset+3,1):uint())
                
            elseif header_identifier == 0x2000 then
            
                local lmc_rawdata_info = subtree:add(p_spead, buffer(offset,8), "LMC raw data info") 
                lmc_rawdata_info:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                lmc_rawdata_info:add(f_header_identifier, buffer(offset,2):uint())
                lmc_rawdata_info:add(f_start_ant_id, buffer(offset+6,1):uint())
                lmc_rawdata_info:add(f_nof_included_ant, buffer(offset+7,1):uint())
            else
                local unknown_tree = subtree:add(p_spead, buffer(offset,8), "Unknown Header Item")
                unknown_tree:add(f_header_identifier_start,  math.floor(buffer(offset,2):uint() / 2^15))
                unknown_tree:add(f_header_identifier, buffer(offset,2):uint())
                unknown_tree:add(f_header_contents_top, buffer(offset+2,2):uint())
                unknown_tree:add(f_header_contents_bottom, buffer(offset+4,4):uint())
            end
            
            

        end
    end
end

local udp_table = DissectorTable.get("udp.port")
udp_table:add(4660, p_spead)
