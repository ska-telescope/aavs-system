from setuptools import setup, find_packages

setup(
    name='aavs-system',
    version='2.1.5',
    packages=find_packages(),
    url='https://gitlab.com/ska-telescope/aavs-system',
    license='',
    author='Alessio Magro',
    author_email='alessio.magro@um.edu.mt',
    description='AAVS Software',
    install_requires = [],  # See requirements file
)
