import sys
sys.path.append('../')

from utilities.skalab.hardware_client import WebHardwareClient
from time import sleep

# TODO: Add method to report which slots are ON or OFF

_RAL_SUBRACK_1_IP = "10.132.0.14"
_RAL_SUBRACK_2_IP = "10.132.0.34"
_RAL_SUBRACK_3_IP = "10.132.0.54"
_RAL_SUBRACK_4_IP = "10.132.0.74"

class Subrack:
    def __init__(self, ip):
        self._ip = ip
      
    def connect_to_subrack(self):
        client = WebHardwareClient(self._ip, "8081")
        if not client.connect():
            print("Error: Could not connect to subrack")
            return None
        print(f"Connected to subrack {self._ip}:8081")
        return client

    def power_on_all_tpms(self):
        client = self.connect_to_subrack()
        if client:
            while True:
                print("Issuing command to subrack...")
                ret = client.execute_command(command="turn_on_tpms")
                print(f"Subrack Returned: {ret}")
                if ret['status'] != 'BUSY':
                    break
                print("Retrying...")
                sleep(2)
            print("All TPMs are now ON!")

    def power_off_all_tpms(self):
        client = self.connect_to_subrack()
        if client:
            while True:
                print("Issuing command to subrack...")
                ret = client.execute_command(command="turn_off_tpms")
                print(f"Subrack Returned: {ret}")
                if ret['status'] != 'BUSY':
                    break
                print("Retrying...")
                sleep(2)
            print("All TPMs are now OFF!")

    def power_cycle_all_tpms(self):
        self.power_off_all_tpms()
        self.power_on_all_tpms()
        print("Request complete!")

    def power_on_tpm(self, slot_list):
        client = self.connect_to_subrack()
        if client:
            for slot in slot_list:
                if not isinstance(slot, int) or int(slot) > 8 or int(slot) < 1:
                    print(f"Error: slot number must be an integer 1-8. Got {slot}")
                    continue
                while True:
                    print("Issuing command to subrack...")
                    ret = client.execute_command(command="turn_on_tpm", parameters=slot)
                    print(f"Subrack Returned: {ret}")
                    if ret['status'] != 'BUSY':
                        break
                    print("Retrying...")
                    sleep(2)
                print(f"TPM {slot} is now ON!")
            print("Power ON Request complete!")

    def power_off_tpm(self, slot_list):
        client = self.connect_to_subrack()
        if client:
            for slot in slot_list:
                if not isinstance(slot, int) or int(slot) > 8 or int(slot) < 1:
                    print(f"Error: slot number must be an integer 1-8. Got {slot}")
                    continue
                while True:
                    print("Issuing command to subrack...")
                    ret = client.execute_command(command="turn_off_tpm", parameters=slot)
                    print(f"Subrack Returned: {ret}")
                    if ret['status'] != 'BUSY':
                        break
                    print("Retrying...")
                    sleep(2)
                print(f"TPM {slot} is now OFF!")
            print("Power OFF Request complete!")

    def power_cycle_tpm(self, slot_list):
        self.power_off_tpm(slot_list)
        self.power_on_tpm(slot_list)

    def set_fan_speed(self, speed=80):
        client = self.connect_to_subrack()
        if client:
            for i in range(4):
                while True:
                    print("Issuing command to subrack...")
                    ret = client.execute_command(command="set_fan_mode", parameters=f"{i+1},0")
                    print(f"Subrack Returned: {ret}")
                    if ret['status'] != 'BUSY':
                        break
                    print("Retrying...")
                    sleep(2)
                print(f"Subrack Fan {i+1} speed set to MANUAL")
                sleep(0.5)
                while True:
                    print("Issuing command to subrack...")
                    ret = client.execute_command(command="set_subrack_fan_speed", parameters=f"{i+1},{speed}")
                    print(f"Subrack Returned: {ret}")
                    if ret['status'] != 'BUSY':
                        break
                    print("Retrying...")
                    sleep(2)
                print(f"Subrack Fan {i+1} speed set to {speed}%")


class RALSubrack1(Subrack):
    def __init__(self):
        super().__init__(ip=_RAL_SUBRACK_1_IP)

class RALSubrack2(Subrack):
    def __init__(self):
        super().__init__(ip=_RAL_SUBRACK_2_IP)

class RALSubrack3(Subrack):
    def __init__(self):
        super().__init__(ip=_RAL_SUBRACK_3_IP)

class RALSubrack4(Subrack):
    def __init__(self):
        super().__init__(ip=_RAL_SUBRACK_4_IP)

