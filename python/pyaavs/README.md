# Deprecated Code!

This version of the following files is now deprecated. As of January 2025 development has moved to [SKA-Low SPS Test Suite](https://gitlab.com/ska-telescope/ska-low-sps-testsuite):
- tests/
- station.py

The following files are still being developed but will soon be migrated to [SKA-Low SPS TPM API](https://gitlab.com/ska-telescope/ska-low-sps-tpm/ska-low-sps-tpm-api):
- plugins/
- tile.py
- tile_health_monitor.py
- tpm_monitoring_point_lookup.py



