# Deprecated Code!

This version of the tests is now deprecated. As of January 2025 development has moved to [SKA-Low SPS Test Suite](https://gitlab.com/ska-telescope/ska-low-sps-testsuite).


