# Version History

### 2.1.5
* [JANUS-83] Add validation of user input for tile 40G subnet

## 2.1.4
* [SPRTS-260] AAVSFileManager in aavs_file.py fails when trying to lock a read-only for read-only access

## 2.1.3

* [THORN-5] Fix tile.test_generator_set_delay

## 2.1.2
* [JANUS-65] Clean up HDF5 files after hardware tests

## 2.1.1

* [JANUS-44] Minor DAQ changes from migrating SKA-Low SPS Test Suite to new repo 
* [JANUS-63] Fix to CI Pipeline following SKA System Team breaking change 
* [JANUS-46] & [JANUS-62]: Correct any issues with SKALAB arising from AAVS-System 2.0.0 release & Fixes to tests 
* [JANUS-61] Remove unused files
* [THORN-10] Update AavsFile.get_metadata() to fetch observation info.

## 2.1.0
* [MCCS-2148] Implemented new Antenna Buffer software interface in AAVS-System

## 2.0.1
* [MCCS-2163] Added support for designs not named 'tpm_test'

## 2.0.0
* [MCCS-2164] Remove references to TPM 1.2 in the AAVS System and PyFABIL repository

## 1.5.1
* [MCCS-2162] Add hardware tests to AAVS System CI, and added auto firmware downloader
* [REL-1824] Added TPM FPGA Firmware Bitstreams v5.5.0 multi channel and v6.2.0

## 1.5.0
* [MCCS-1556] Added tile level methods for station beam flagging

## 1.4.2
* [MCCS-2328] Expose enabling/disabling ADCs
* [MCCS-2325] Update logging for failure to connect to board
* [REL-1824] Added TPM FPGA Firmware Bitstreams v5.5.0 and v6.1.0

## 1.4.1
* [SKB-610] Add StationID to daq config and add set StationID in metadata calls.

## 1.4.0
* [MCCS-2232] Add station beamformer discarded packet counters to health monitoring

## 1.3.2
* [SKB-510] Fix to tile.info and tile.__str__ takes 3 seconds to return
* [MCCS-2308] Improvements to AAVS-System hardware regression tests

## 1.3.1
* [SKB-610] Add storage of Station ID in persisters to get it into datafiles

## 1.3.0
* [MCCS-2025] Added methods to monitor and control broadband RFI counters

## 1.2.6
* [MCCS-2273] Fix continuous channel data bug

## 1.2.5
* [MCCS-2300] Expose the pattern generator to MCCS
* [TOP-1577] Simplify deploy script for permission denied
* [MCCS-2282] Support for NumPy 2.X
* [MCCS-2281] Pinned Numpy to 1.24.4

## 1.2.4
* [MCCS-2247] Added sychronisation and bandpass tests to regression test suite
* [SKB-526] Fix to order of channeliser truncation

## 1.2.3
* [MCCS-2204] Produce Custom Summary for Hardware Tests in Firmware CI
* [SKB-433] Abort all LMC data requests in tile.stop_transmission()
* [SPRTS-240] Correcting misleading check_pll_locked() method
* [MCCS-2011] Changes to SKALAB following subrack API 2.6.0
* [MCCS-1979] Refactor TPM UDP Health Monitoring following Single 40G Firmware
* [SKB-426] Removed unused voltages VM_ADA0 and VM_ADA1

## 1.2.2
* [SPRTS-230] 200 MHz is zero in grafana integrated power (bandpass) plots

## 1.2.1
* [MCCS-2218] Fix 16 TPMs not synchronizing with latest firmware 5.4.0

## 1.2.0
* [SP-3800] Complete the implementation of ECP-230134, updating SPS-CBF SPEAD time fields
